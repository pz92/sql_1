-- PIOTR ZEJLER SPRAWOZDANIE --
-- UBEZPIECZENIA SAMOCHODOWE --

DROP TABLE POLISA_SAMOCHOD_BACKUP;
DROP TABLE POLISA_SAMOCHOD;
DROP TABLE KIEROWCA_SAMOCHOD;
DROP TABLE SAMOCHOD;
DROP TABLE TYP_POLISY;
DROP TABLE KIEROWCY;

-- TABELA ZAWIERA DANE KIEROWCÓW KTÓRZY MAJĄ WYKUPIONĄ POLISĘ
CREATE TABLE KIEROWCY(
    id_kierowcy NUMBER CONSTRAINT kierowcy_pk PRIMARY KEY,
    imie VARCHAR2(100) NOT NULL,
    nazwisko VARCHAR2(100) NOT NULL,
    pesel NUMBER(11) CONSTRAINT kierowcy_un UNIQUE NOT NULL,
    data_ur DATE,
    nr_tel NUMBER NOT NULL
);

-- TABELA ZAWIERA WSZYSTKIE TYPY POLIS JAKIE MOGĄ BYĆ PRZYPISANE DO SAMOCHODU
CREATE TABLE TYP_POLISY(
    id_polisy NUMBER CONSTRAINT typ_polisy_pk PRIMARY KEY,
    rodzaj_polisy VARCHAR2(50) CONSTRAINT typ_polisy_un UNIQUE NOT NULL,
    cena REAL NOT NULL
);

-- TABELA ZAWIERA SAMOCHODY NALEŻĄCE DO KIEROWCÓW
CREATE TABLE SAMOCHOD(
    id_samochodu VARCHAR2(7) CONSTRAINT samochod_pk PRIMARY KEY,
    marka_samochodu VARCHAR2(50) NOT NULL,
    model_samochodu VARCHAR2(50) NOT NULL,
    rocznik_samochodu NUMBER(4) NOT NULL,
    numer_vin VARCHAR2(17) CONSTRAINT rejestracje_un UNIQUE NOT NULL
);

--  TABELA ZAWIERA POWIĄZANIA MIĘDZY TABELĄ KIEROWCY I SAMOCHOD, UMOŻLIWIA TO PRZYPISANIE DO JEDNEGO KIEROWCY KILKU SAMOCHODÓW
CREATE TABLE KIEROWCA_SAMOCHOD(
    id_kierowcy NUMBER NOT NULL,
    id_samochodu VARCHAR2(7) PRIMARY KEY,
    CONSTRAINT id_sam_kierowca_samochod_fk FOREIGN KEY(id_samochodu) REFERENCES SAMOCHOD(id_samochodu),
    CONSTRAINT id_kier_kierowca_samochod_fk FOREIGN KEY(id_kierowcy) REFERENCES KIEROWCY(id_kierowcy)
);

-- TABELA ZAWIERA POWIĄZANIA MIĘDZY TABELĄ POLISA I SAMOCHOD, DO SAMOCHODU MOŻLIWE JEST PRZYPISANIE POLISY OC, AC, ASS, NWW, SZYB
-- KAŻDA POLISA MOŻE MIEĆ RÓŻNĄ DŁUGOŚĆ TRWANIA
CREATE TABLE POLISA_SAMOCHOD(
    id_polisy NUMBER NOT NULL,
    id_samochodu VARCHAR2(7) NOT NULL,
    data_polisy DATE NOT NULL,
    CONSTRAINT id_pol_polisa_samochod_fk FOREIGN KEY(id_polisy) REFERENCES TYP_POLISY(id_polisy),
    CONSTRAINT id_sam_polisa_samochod_fk FOREIGN KEY(id_samochodu) REFERENCES SAMOCHOD(id_samochodu)
);

-- WYZWALACZE

-- WYZWALACZ ZAPOBIEGAJĄCY PRZYPISANIU DO JEDNEGO SAMOCHODU WIĘCEJ NIŻ JEDNEJ POLISY TEGO SAMEGO TYPU
-- UNIEMOŻLIWIA PRZYPISANIE DO JEDNEGO AUTA NP. DWÓCH UBEZPIECZEŃ "OC"
-- W RAZIE PRÓBY PRZYPISANIA TEGO SAMEGO RODZAJU UBEZPIECZENIA, POJAWI SIĘ INFORMACJA WYJĄTKU
CREATE OR REPLACE TRIGGER SPRAWDZ_POLISY_SAMOCHODU
BEFORE INSERT ON POLISA_SAMOCHOD
FOR EACH ROW
DECLARE
    flag NUMBER; 
    wyjatek EXCEPTION;
BEGIN
    SELECT COUNT(*) INTO flag FROM POLISA_SAMOCHOD 
    WHERE ID_SAMOCHODU = :new.id_samochodu AND ID_POLISY = :new.id_polisy;
    
    IF flag > 0 THEN 
        RAISE wyjatek;
    END IF;
    
    EXCEPTION
    WHEN wyjatek THEN
        Raise_Application_Error (-20099, 'Samochoód posiada już tę polisę!');
END SPRAWDZ_POLISY_SAMOCHODU;
/

-- WYZWALACZ, KTÓRY AUTOAMATYCZNIE WYCIĄGNIE DATĘ URODZANIA Z PODANEGO NUMERU PESEL I WPISZE W ODPOWIEDNIE POLE
CREATE OR REPLACE TRIGGER PESEL_DO_DATA
AFTER INSERT ON KIEROWCY
DECLARE
    d_ur DATE;
    id_k NUMBER(6);
BEGIN 
    SELECT max(id_kierowcy) INTO id_k FROM KIEROWCY;
    SELECT substr(PESEL,1,6) INTO d_ur FROM KIEROWCY WHERE id_kierowcy = id_k;
    UPDATE KIEROWCY SET data_ur = d_ur WHERE id_kierowcy = id_k;
END PESEL_DO_DATA;

/
-- FUNKCJE
-- FUNKCJA ZWRACAJĄCA ZSUMOWANE UBEZPIECZENIE SAMOCHODU, KTÓREGO NUMER REJESTRACYJNY ZOSTAŁ PODANY JAKO ARGUMENT PODCZAS WYWOŁANIA
CREATE OR REPLACE FUNCTION CENA_UBEZPIECZENIA(id_sam IN VARCHAR2) RETURN FLOAT
IS
    cena REAL;
BEGIN
    FOR x IN (select sum(tp.cena) as cena_ubezpieczenia from polisa_samochod ps
                join typ_polisy tp on 
                    ps.id_polisy = tp.id_polisy
                where ps.id_samochodu = id_sam
                group by ps.id_samochodu) LOOP
        cena := x.cena_ubezpieczenia;
    END LOOP;
    RETURN cena;
END CENA_UBEZPIECZENIA;
/

-- FUNKCJA ZWRACAJĄCA ILOŚĆ DNI DO KOŃCA TRWANIA KAŻDEJ POLISY SAMOCHODU, KTÓREGO NUMER REJESTRACYJNY i RODZAJ POLISY ZOSTAŁY PODANE JAKO ARGUMENTY PODCZAS WYWOŁANIA
CREATE OR REPLACE FUNCTION KONIEC_POLISY(id_sam IN VARCHAR2, id_pol IN NUMBER) 
RETURN INT
IS
    ile INT;
BEGIN
    SELECT round(ABS(CURRENT_DATE - data_polisy)) INTO ile FROM polisa_samochod 
    WHERE id_samochodu = id_sam AND id_polisy = id_pol;
    RETURN ile;
END KONIEC_POLISY;
/

-- PROCEDURY 
-- PROCEDURA POZWALAJĄCA NA DODANIE DO BAZY NOWEGO KIEROWCY I JEGO SAMOCHODU
-- PODCZAS WYWOŁANIA NALEŻY PODAĆ ODPOWIEDNIE ARGUMENTY
-- DO TABELI KIEROWCY ZOSTANIE DODANY KIEROWCA
-- DO TABLI SAMOCHOD ZOSTANIE DODANY SAMOCHOD
-- DO TABELI KIEROWCA_SAMOCHOD ZOSTANIE DODANE POWIĄZANIE KIEROWCY Z SAMOCHODEM
-- PROCEDURA ZAWIERA KURSOR, Z KTÓREGO POBIERANE SĄ INFORMACJE O DODANYM KIEROWCY I SĄ ONE WYŚWIETLANE W DBMS_OUTPUT
CREATE OR REPLACE PROCEDURE DODAJ_KIEROWCE(
    imie VARCHAR2,
    nazwisko VARCHAR2, 
    pesel NUMBER, 
    nr_tel NUMBER,
    nr_rejestracji VARCHAR2,
    marka_samochodu VARCHAR2,
    model_samochodu VARCHAR2,
    rocznik_samochodu NUMBER,
    numer_vin VARCHAR2
    )
IS
    identyfikator NUMBER;
    CURSOR kursor 
    IS
    SELECT id_kierowcy, imie, nazwisko, data_ur, nr_tel FROM KIEROWCY 
    WHERE id_kierowcy = identyfikator; 
    k_id NUMBER;
    k_imie VARCHAR2(100);
    k_nazwisko VARCHAR2(100);
    k_data_ur DATE;
    k_nr_tel NUMBER;
BEGIN
    SELECT MAX(id_kierowcy) INTO identyfikator FROM KIEROWCY;
    identyfikator := identyfikator + 1;
    INSERT INTO KIEROWCY VALUES(identyfikator, imie, nazwisko, pesel, 
    NULL, nr_tel);
    INSERT INTO SAMOCHOD VALUES(nr_rejestracji, marka_samochodu, 
    model_samochodu, rocznik_samochodu, numer_vin);
    INSERT INTO KIEROWCA_SAMOCHOD VALUES(identyfikator, nr_rejestracji);
    
    OPEN kursor;
    FETCH kursor INTO k_id, k_imie, k_nazwisko, k_data_ur, k_nr_tel;
    CLOSE kursor;
    dbms_output.put_line('Do tabeli KIEROWCY został dodany nowy kierowca:');
    dbms_output.put_line(k_id || ' ' || k_imie || ' ' || k_nazwisko || ' ' 
    || k_data_ur || ' ' || k_nr_tel);
END DODAJ_KIEROWCE;
/

-- PROCEDURA POZWALAJĄCA NA DODANIE DO SAMOCHODU UBEZPIECZENIA
-- PODCZAS WYWOŁANIA NALEŻY PODAĆ NAZWĘ POLISY: OC, AC, ASS, NWW, SZYB
-- DŁUGOŚĆ POLISY USTALANY WPISUJĄC ILOŚĆ MIESIĘCY
-- PROCEDURA ZAWIERA OBSŁUGĘ WYJĄTKÓW, JEŚLI ZOSTANIE PODANY NUMER REJESTRACJI, KTÓREGO NIE MA W BAZIE SAMOCHODÓW, POJAWI SIĘ STOSOWNA INFORMACJA O BŁĘDZIE
-- PROCEDURA ZAWIERA KURSOR, Z KTÓREGO PO DODANIU POLISY DO AUTA ZOSTANIE POBRANA INFORMACJA JAKIE POLISY SĄ AKTUALNIE PRZYPISANE DO DANEGO AUTA
CREATE OR REPLACE PROCEDURE DODAJ_POLISE(
    nazwa_polisy VARCHAR2,
    rejestracja VARCHAR2,
    dlogosc_polisy NUMBER
    )
IS
    id_polisa NUMBER;
    flag NUMBER;
    wyjatek EXCEPTION;

    data_polisy DATE;
    CURSOR kursor 
    IS
    SELECT LISTAGG(tp.rodzaj_polisy,';') WITHIN GROUP(ORDER BY tp.rodzaj_polisy) 
    FROM polisa_samochod ps 
        JOIN typ_polisy tp ON ps.id_polisy = tp.id_polisy 
    WHERE ps.id_samochodu = rejestracja;    
    polisy VARCHAR(100);
BEGIN
    SELECT tp.id_polisy INTO id_polisa FROM TYP_POLISY tp 
    WHERE tp.rodzaj_polisy = nazwa_polisy;
    data_polisy := ADD_MONTHS(current_date, dlogosc_polisy);
    SELECT count(*) INTO flag FROM SAMOCHOD WHERE id_samochodu = rejestracja;
    IF flag <= 0 THEN 
        RAISE wyjatek;
    END IF;

    INSERT INTO POLISA_SAMOCHOD VALUES(id_polisa, rejestracja, data_polisy);
    OPEN kursor;
    FETCH kursor INTO polisy;
    CLOSE kursor;
    dbms_output.put_line('Samochód o numerze rejestracyjnym ' || rejestracja || 
    ' posiada wykupione natępujące polisy: ' || polisy);
    
    EXCEPTION
    WHEN wyjatek THEN
        Raise_Application_Error (-20121, 'Nie ma takego samochodu w tabeli 
        SAMOCHOD!');
END DODAJ_POLISE;
/  

-- DYNAMICZNY SQL
-- PROCEDURA POZWALAJACA NA WYCZYSZCZENIE TABELI, KTÓREJ NAZWA ZOSTANIE PODANA JAKO ARGUMENT PODCZAS WYWOŁANIA
CREATE OR REPLACE PROCEDURE WYCZYSC_TABELE(
    nazwa_tabeli VARCHAR2
)
IS
BEGIN
    EXECUTE IMMEDIATE 'TRUNCATE TABLE ' || nazwa_tabeli;
END WYCZYSC_TABELE;
/

-- PROCEDURA, DO KTÓREJ ODNOSI SIĘ WYZWALACZ. TWORZY ONA NOWĄ TABELĘ
/  
CREATE OR REPLACE PROCEDURE BACKUP_POLISY
IS
    PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
    EXECUTE IMMEDIATE 
        'CREATE TABLE POLISA_SAMOCHOD_BACKUP(
        id_polisy NUMBER NOT NULL,
        id_samochodu VARCHAR2(7) NOT NULL,
        data_polisy DATE NOT NULL)';
END;
/

-- WYZWALACZ, KTÓRY PRZED USUNIĘCIEM REKRODRU Z TABELI POLISA_SAMOCHOD, KOPIUJE GO DO TABELI POLISA_SAMOCHOD_BACKUP.
-- JEŚLI TABELA TA NIE ISTNIEJE, WYZWALACZ URUCHAMIA PROCEDURĘ, KTÓRA TWORZY TAKĄ TABELĘ I NASTĘPNIE KOPIUJE KASOWANY REKORD.
CREATE OR REPLACE TRIGGER USUN_POLISA_SAMOCHOD
BEFORE DELETE ON POLISA_SAMOCHOD
FOR EACH ROW
DECLARE 
    flag NUMBER;
BEGIN
    select count(tname) INTO flag from tab where tname='POLISA_SAMOCHOD_BACKUP';
    IF flag < 1 THEN
        BACKUP_POLISY; 
    END IF;
    EXECUTE IMMEDIATE 
        'INSERT INTO POLISA_SAMOCHOD_BACKUP VALUES (:1, :2, :3)'
    USING :old.id_polisy, :old.id_samochodu, :old.data_polisy;
END;
/


-- PRZYKŁADOWE DANE, KTÓRE NALEŻY WCZYTAĆ
INSERT INTO KIEROWCY VALUES(1, 'Adam', 'Nowak', 89071609323, NULL, 609389220);
INSERT INTO KIEROWCY VALUES(2, 'Tomasz', 'Kowalski', 87070102847, NULL, 502989124);
INSERT INTO KIEROWCY VALUES(3, 'Agata', 'Kwiatkowski', 88011418212, NULL, 789433084);
INSERT INTO KIEROWCY VALUES(4, 'Kinga', 'Adamiecka', 90071316182, NULL, 698432098);
INSERT INTO KIEROWCY VALUES(5, 'Marlena', 'Tatarek', 90091212749, NULL, 511098229);
INSERT INTO KIEROWCY VALUES(6, 'Adrian', 'Kępa', 92100800936, NULL, 501998333);
INSERT INTO KIEROWCY VALUES(7, 'Rafał', 'Utratny', 92040910784, NULL, 779022432);
INSERT INTO KIEROWCY VALUES(8, 'Piotr', 'Mickiewicz', 88111205987, NULL, 888231094);
INSERT INTO KIEROWCY VALUES(9, 'Monika', 'Dunaj', 92090309226, NULL, 778298111);
INSERT INTO KIEROWCY VALUES(10, 'Krystian', 'Nowakowski', 92090514594, NULL, 609287144);
--select * from KIEROWCY;

INSERT INTO TYP_POLISY VALUES(1, 'OC', 800);
INSERT INTO TYP_POLISY VALUES(2, 'AC', 1400);
INSERT INTO TYP_POLISY VALUES(3, 'ASS', 400);
INSERT INTO TYP_POLISY VALUES(4, 'NNW', 300);
INSERT INTO TYP_POLISY VALUES(5, 'SZYB', 200);
--select * from TYP_POLISY;

INSERT INTO SAMOCHOD VALUES('SC36406', 'Audi', 'A8', 2009, 'WVWZZZ3BZ1P271787');
INSERT INTO SAMOCHOD VALUES('WW87732', 'Mercedes', 'A180', 2014, '2G1WT57K591205488');
INSERT INTO SAMOCHOD VALUES('SKL4355', 'Peugeot', '406', 2002, 'WAURFAFR1AA094366');
INSERT INTO SAMOCHOD VALUES('PKA9833', 'Opel', 'Astra', 2017, '1GCGC34JXWF009420');
INSERT INTO SAMOCHOD VALUES('CIN3422', 'Nissan', 'Micra', 2000, '1FDXD70U6DVA39162');
INSERT INTO SAMOCHOD VALUES('LPA2351', 'Audi', 'A2', 1999, '1G2JB524817295129');
INSERT INTO SAMOCHOD VALUES('SCZ1233', 'Volkswagen', 'Golf', 2009, '1HD4CP2187K489316');
INSERT INTO SAMOCHOD VALUES('SMI9232', 'Mercedes', 'C220', 2008, '1FTDE14YXKHB09823');
INSERT INTO SAMOCHOD VALUES('SRS9999', 'BMW', '5', 2011, '1FMRE11L3YHB31858');
INSERT INTO SAMOCHOD VALUES('SD11942', 'Toyota', 'Corolla', 1998, '1FVABPCS03DM12747');
INSERT INTO SAMOCHOD VALUES('SRI9112', 'Seat', 'Ibiza', 2008, 'WDZPE7CC4A5462676');
INSERT INTO SAMOCHOD VALUES('SRB2399', 'Suzuki', 'Swift', 2001, '1G1ZH57B284231144');
INSERT INTO SAMOCHOD VALUES('WW34333', 'Fiat', 'Tipo', 2016, '2T1BB02E3VC163556');
--select * from SAMOCHOD;

INSERT INTO KIEROWCA_SAMOCHOD VALUES(1, 'SC36406');
INSERT INTO KIEROWCA_SAMOCHOD VALUES(2, 'WW87732');
INSERT INTO KIEROWCA_SAMOCHOD VALUES(2, 'SKL4355');
INSERT INTO KIEROWCA_SAMOCHOD VALUES(3, 'PKA9833');
INSERT INTO KIEROWCA_SAMOCHOD VALUES(4, 'CIN3422');
INSERT INTO KIEROWCA_SAMOCHOD VALUES(4, 'LPA2351');
INSERT INTO KIEROWCA_SAMOCHOD VALUES(5, 'SCZ1233');
INSERT INTO KIEROWCA_SAMOCHOD VALUES(6, 'SMI9232');
INSERT INTO KIEROWCA_SAMOCHOD VALUES(6, 'SRS9999');
INSERT INTO KIEROWCA_SAMOCHOD VALUES(7, 'SD11942');
INSERT INTO KIEROWCA_SAMOCHOD VALUES(8, 'SRI9112');
INSERT INTO KIEROWCA_SAMOCHOD VALUES(9, 'SRB2399');
INSERT INTO KIEROWCA_SAMOCHOD VALUES(10, 'WW34333');
--select * from KIEROWCA_SAMOCHOD;

INSERT INTO POLISA_SAMOCHOD VALUES(1, 'SC36406', '2017-12-03');
INSERT INTO POLISA_SAMOCHOD VALUES(2, 'SC36406', '2017-06-03');
INSERT INTO POLISA_SAMOCHOD VALUES(3, 'SC36406', '2017-12-03');
INSERT INTO POLISA_SAMOCHOD VALUES(1, 'WW87732', '2018-04-23');
INSERT INTO POLISA_SAMOCHOD VALUES(1, 'SKL4355', '2018-02-28');
INSERT INTO POLISA_SAMOCHOD VALUES(5, 'SKL4355', '2017-08-28');
INSERT INTO POLISA_SAMOCHOD VALUES(1, 'PKA9833', '2017-09-16');
INSERT INTO POLISA_SAMOCHOD VALUES(1, 'CIN3422', '2018-03-13');
INSERT INTO POLISA_SAMOCHOD VALUES(2, 'CIN3422', '2017-09-13');
INSERT INTO POLISA_SAMOCHOD VALUES(1, 'LPA2351', '2017-07-01');
INSERT INTO POLISA_SAMOCHOD VALUES(1, 'SCZ1233', '2017-10-01');
INSERT INTO POLISA_SAMOCHOD VALUES(1, 'SMI9232', '2017-12-31');
INSERT INTO POLISA_SAMOCHOD VALUES(1, 'SRS9999', '2017-06-21');
INSERT INTO POLISA_SAMOCHOD VALUES(2, 'SRS9999', '2017-06-21');
INSERT INTO POLISA_SAMOCHOD VALUES(3, 'SRS9999', '2017-06-21');
INSERT INTO POLISA_SAMOCHOD VALUES(4, 'SRS9999', '2017-06-21');
INSERT INTO POLISA_SAMOCHOD VALUES(5, 'SRS9999', '2017-06-21');
INSERT INTO POLISA_SAMOCHOD VALUES(1, 'SD11942', '2017-12-22');
INSERT INTO POLISA_SAMOCHOD VALUES(1, 'SRI9112', '2017-11-09');
INSERT INTO POLISA_SAMOCHOD VALUES(1, 'SRB2399', '2017-09-03');
INSERT INTO POLISA_SAMOCHOD VALUES(3, 'SRB2399', '2017-09-03');
INSERT INTO POLISA_SAMOCHOD VALUES(1, 'WW34333', '2017-08-01');
--select * from POLISA_SAMOCHOD;

-- PRZYKŁADOWE WYWOŁANIA FUNKCJI, PROCEDUR
-- DODANIE NOWEGO KIEROWCY WRAZ Z SAMOCHODEM
-- KOLEJNO NALEŻY PODAĆ:
-- IMIĘ, NAZWISKO, PESEL, NUMER TELEFONU, NUMER REJESTRACJI, MARKA, MODEL, ROK PRODUKCJI, NUMER VIN
EXEC DODAJ_KIEROWCE('Marcelina', 'Zawadzka', 95030110405, 600777888, 'SC12345', 'Opel', 'Vectra', 2007, '1C6RD7LT7CS148480');

-- delete from kierowca_samochod value where id_samochodu = 'SC12345';
-- delete from samochod value where id_samochodu = 'SC12345';
-- delete from kierowcy value where nazwisko = 'Zawadzka';

-- DODANIE POLISY DO SAMOCHODU
-- KOLEJNO NALEŻY PODAĆ:
-- RODZAJ POLISY (OC, AC, ASS, NWW, SZYB), NUMER REJESTRACJI, ILOŚĆ MIESIĘCY (np. 6 - OZNACZA PÓŁ ROKU)

EXEC DODAJ_POLISE('OC', 'SC12345', 6);

-- WYŚWIETLENIE KIEROWCÓW, ICH SAMOCHODÓW I PRZYPISANYCH POLIS
SELECT k.imie, k.nazwisko, k.data_ur, s.id_samochodu as rejestracja, s.marka_samochodu as marka, s.model_samochodu as "MODEL", LISTAGG(tp.rodzaj_polisy, ';') WITHIN GROUP(ORDER BY tp.rodzaj_polisy) AS "WYKUPIONE POLISY", LISTAGG(tp.cena, ';') WITHIN GROUP(ORDER BY tp.rodzaj_polisy) as "CENY POLIS" 
FROM KIEROWCY k
JOIN KIEROWCA_SAMOCHOD ks ON
    k.id_kierowcy = ks.id_kierowcy
JOIN SAMOCHOD s ON
    ks.id_samochodu = s.id_samochodu
JOIN POLISA_SAMOCHOD ps ON
    s.id_samochodu = ps.id_samochodu
JOIN TYP_POLISY tp ON
    ps.id_polisy = tp.id_polisy
GROUP BY k.imie, k.nazwisko, k.data_ur, s.id_samochodu, s.marka_samochodu, s.model_samochodu
ORDER BY k.nazwisko, s.id_samochodu;
    
-- WYŚWIETLENIE SAMOCHODÓW I KOSZTU UBEZPIECZENIA (OBLICZONEGO FUNKCJĄ)
SELECT id_samochodu, marka_samochodu, model_samochodu, rocznik_samochodu, 
CENA_UBEZPIECZENIA(id_samochodu) AS CENA_UBEZPIECZENIA FROM SAMOCHOD;

-- WYŚWIETLENIE KIEROWCÓW, ICH SAMOCHODÓW WRAZ Z INFORMACJĄ ILE DNI ZOSTAŁO DO KOŃCA POLISY (OBLICZNAE FUNKCJĄ)
SELECT k.imie, k. nazwisko, s.id_samochodu, s.marka_samochodu, s.model_samochodu, 
LISTAGG(tp.rodzaj_polisy, ';') WITHIN GROUP (ORDER BY tp.rodzaj_polisy) 
AS "WYKUPIONE POLISY", LISTAGG(KONIEC_POLISY(ps.id_samochodu, ps.id_polisy), ';') 
WITHIN GROUP (ORDER BY tp.rodzaj_polisy) AS "ILOŚĆ DNI DO KOŃCA POLISY" 
FROM samochod s
JOIN POLISA_SAMOCHOD ps ON
    s.id_samochodu = ps.id_samochodu
JOIN KIEROWCA_SAMOCHOD ks ON
    s.id_samochodu = ks.id_samochodu
JOIN KIEROWCY k ON
    ks.id_kierowcy = k.id_kierowcy
JOIN TYP_POLISY tp ON
    ps.id_polisy = tp.id_polisy
GROUP BY k.imie, k. nazwisko, s.id_samochodu, s.marka_samochodu, s.model_samochodu;

-- WYCZYSZCZENIE TABELI PRZY UŻYCIU STWORZONEJ PROCEDURY
EXEC WYCZYSC_TABELE('KIEROWCA_SAMOCHOD');
-- SELECT * FROM KIEROWCA_SAMOCHOD;

-- USUNIĘCIE REKORDU Z TABELI POLISA_SAMOCHOD (ZOSTANIE ON PRZENIESIONY DO TABELI POLISA_SAMOCHOD_BACKUP)
-- DROP TABLE POLISA_SAMOCHOD_BACKUP;
DELETE FROM POLISA_SAMOCHOD VALUE WHERE id_samochodu='SC36406';
-- SELECT * FROM POLISA_SAMOCHOD_BACKUP;
-- SELECT * FROM POLISA_SAMOCHOD;

